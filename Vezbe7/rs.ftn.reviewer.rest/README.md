# Mobilne aplikacije (2022/2023)

**Pokretanje aplikacije pomoću Eclipse IDE for Java EE Developers i MySQL Workbench**

1. Otvoriti projekat `File/Open Projects from File System` 
2. Podesiti verziju Jave u okviru `Window/Preferences` 
    1. Java Build Path v1.8
    2. Java Compiler v1.8
    3. Installed JRE - podesiti putanju do v1.8 na računaru
3. Podesiti server - Apache Tomcat v7.0
    1. Servers kartica - add new server
    2. Instalirati apache Tomcat v7.0 
    3. Podesiti JRE v1.8
    4. Dodati naš importovati projekat da se pokreće na serveru
4. Podešavanje Jave u okviru projekat
    1. Desni klik na projekat `Build Path/Configure Build Path`
    2. Java Build Path/Libraries/JRE system library - 1.8
    3. Java Build Path/Libraries/Server Runtime - Apache Tomcat v7.0
    4. Java Compiler v1.8
    5. Project Facets/java v1.8
    6. Targeted Runtimes - Apache Tomcat v7.0
5. Podešavanje baze 
    1. Opcija u okviru MySQL Workbench aplikaciji `Create a new schema in the connected server`
    2. Schema name `android`
    3. U okviru `hibernate.cfg.xml` fajla na serverskoj strani podesiti konekciju ka bazi 
    ```
        <property name="hibernate.connection.url"> jdbc:mysql://localhost:3306/android</property>
        <property name="hibernate.connection.username">[username]</property>
        <property name="hibernate.connection.password">[password]</property>
    ```
    4. U okviru /service/ServiceUtils podesiti umesto [adresa] svoju IP adresu na računaru. 
6. Pokretanje servera: Servers kartica - desni klik, opcija start
7. Pokretanje test primera na serverskoj strani 
    1. Desni klik na `rs.ftn.reviewer.rest.util/Test`, opcija Run As Java Application
    2. Desni klik na `rs.ftn.reviewer.rest.util/Test`, opcija Run As Java Application

