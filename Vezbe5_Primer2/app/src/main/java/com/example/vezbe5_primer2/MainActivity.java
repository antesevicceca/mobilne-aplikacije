package com.example.vezbe5_primer2;

import static com.example.vezbe5_primer2.util.NetworkUtil.fetchImage;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.ImageView;

import com.example.vezbe5_primer2.util.NetworkUtil;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainActivity extends AppCompatActivity {

    String mUrl = "https://www.pametnitelefoni.rs/images/main/2--33.jpeg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_layout);

        ImageView imageView = findViewById(R.id.image);
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());

        executorService.execute(new Runnable() {
            @Override
            public void run() {
                Log.i("REZ", "Background work here");
                Bitmap bitmap = NetworkUtil.fetchImage(mUrl);
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        if(bitmap != null){
//                            imageView.setImageBitmap(bitmap);
//                        }
//                    }
//                });
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if(bitmap != null){

                            Log.i("REZ", "UI Thread work here");
                            imageView.setImageBitmap(bitmap);
                        }
                    }
                });

            }
        });

    }
}