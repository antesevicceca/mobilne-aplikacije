# Mobilne aplikacije (2022/2023)

Materijali za vežbe na predmetu **Mobilne aplikacije (MA)** na smeru **Softversko inženjerstvo i informacione tehnologije (SIIT)**, Fakultet tehničkih nauka, Novi Sad. 

[![Ftn logo](ftn-logo.png)](http://www.ftn.uns.ac.rs/691618389/fakultet-tehnickih-nauka)
